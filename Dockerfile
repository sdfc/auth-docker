FROM reciforous/php8.0-fpm:apache2
RUN apt-get update
RUN apt-get install -y php-pgsql
RUN phpenmod pdo_pgsql
RUN rm -f /etc/php/8.0/apache2/php.ini
COPY php.ini /etc/php/8.0/apache2/php.ini
RUN a2enmod ssl
